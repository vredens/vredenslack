# VredenSlack - A post-dist for Slackware

## What is a Post-Dist

A post-dist is a collection of scripts and tools that help you setup your system after the initial installation. It also lets you migrate configurations from one machine to another so you can easily "soft-clone" machines without cloning disks or bringing in trash from one machine to another.

## Requirements

VredenSlack works with Slackware version 13.37 and later.

It assumes you have installed all packages from the `a` and `ap` menus.

It is recommended to have CPANMinus installed. You can easily install CPANMinus on your system's Perl using:

	curl -L http://cpanmin.us | perl - --sudo App::cpanminus

## What does the VredenSlack give me?

  1. A set of tools in the form of small scripts which are installed globally (if you run the installation as root) or for your user only (if that user is not root). These tools are designed to help you in your daily tasks.

  1. Configuration management for your user and system.

  2. A SlackBuilds package manager for installing software from the SlackBuilds website in a similar way to how you install using slackpkg.

## Installing the Tool Scripts

You can install them simply by running

	wget https://gitlab.com/vredens/vredenslack/raw/master/vs-bootstrap.sh
	[sudo] bash vs-bootstrap.sh
	rm vs-bootstrap.sh

This bootstrap script will clone the Vredenslack repository to your disk (requires Git to be installed) and run the installation/update script (`vs-self-update`). Running the `bootstrap.sh` with sudo is optional since you can install the scripts for your user alone, yet installing the scripts globally is recommended.

You will need to install the following Perl modules if you wish to run all scripts

  * `[sudo] cpanm YAML`
  * `[sudo] cpanm JSON`
  * `[sudo] cpanm Capture::Tiny`

### Keeping the scripts up to date

Vredenslack now comes with a self update script, so simply run

	[sudo] vs-self update vscripts

You need to run with root permissions if you installed it globally.

## Configuration Management

This tool is provided by another repository but it is part of the VredenSlack Toolset (VSlack for short). You can install it, however, by running

	[sudo] vs-self install vs-cm

See more at the [VSlack Configuration Manager](http://bitbucket.org/eeriesoftronics/vslack-configuration-manager) project website.

### Setup

There are a few sample configurations located in the [templates](http://bitbucket.org/eeriesoftronics/vslack-configuration-manager/templates/) folder

You can copy these to `/etc/vredenslack/cm/vs-cm.yml` to be accessible to everyone or, if you just want to do backups for your user, place the configuration file in `~/.vredenslack/cm/vs-cm.yml`.

## VSlack Package Manager (vs-pm)

The package manager lets you install/upgrade/uninstall packages from the SlackBuilds.org website. Note that if you install packages manually or from other sources then it is highly recommended to NOT use VredenSlack package manager to mess around with those packages. If you are new to Slackware then we recommend you use VredenSlack and the official slackpkg to handle all your software installations and slowly try to figure out how SlackBuilds work.

To install VSlack Package Manager run

	[sudo] vs-self install vs-pm

## Tool Scripts

A collection of tool scripts for developers, sysadmins or just for the common Slackware user. All scripts are installed under the user's __bin__ folder and have the **vs-** prefix so you can easily check the list of VredenSlack scripts by typing the prefix and double-tap your **tab** key (if you have a shell with autocomplete enabled, of course).

To install the scripts run:

	./vs-scripts

Current installed scripts are:

  * vs-cifs-mount - a tool to manage CIFS mounts and mount/unmount them
  * vs-find - find text inside all files of a given directory tree
  * vs-s2t - convert spaces to tabs (and the reverse) in all files of a given directory tree
  * vs-loghl - a handy tool to highlight logfiles
  * vs-mirror-url - a simple use of lftp to mirror indexed http/ftp URls
  * vs-rename - rename multiple files
  * vs-replace-text - built on top of `vs-find` it lets you replace text inside multiple files
  * vs-sync - a tool to synchronize your current working folder with a remote location (it tries to sync to the same path on the remote location)

All scripts come with a short help or/and a full manual. You can access it by passing the '--help' or '--man' parameters, respectively.

### vs-loghl

Requires perl module YAML::Tiny. Run `cpanm install YAML::Tiny` (you may need root privileges) to install it, or use your own method of installing perl modules.

`vs-loghl` also uses a configuration file. Depending on how you installed (root or regular user) the configuration file is either in

  * `/etc/vredenslack/vs-loghl.yml`
  * `~/.vredenslack/vs-loghl.yml`

Tou should probably take a look at the configuration file. It allows you to define regular expressions to match output lines and a color with which to color them. Also allows to define a start and end flag on a regular expression for when you need to highlight multilines.

You can run it like so:

	tail -F mylog.log anotherlog.log | vs-loghl -a

It also supports creating the tail itself:

	vs-loghl -a mylog.log anotherlog.log

### vs-cifs-mount

Manage CIFS shares. If you are like me and have shares at work, home, friends houses, etc, you can't just have your CIFS shares on fstab or ever worse, some auto discovery tool which is usually slow.

Run it without arguments for more help.

### vs-find

Find text inside all files of a given directory. Run it without arguments for more help.

### vs-s2t

Convert spaces to tabs and vice-versa. I wrote this because I'm a serious advocate of using tabs instead of chosing that silly option on IDE to expand tabs to spaces.

### vs-mirror-url

Mirrors an URL into a folder. Run it without arguments to see a quick help. It can be used to fetch HTTP indexed directories such as Linux package repositories, for example.

### vs-rename

Renames multiple files recursively. It can even remove folders if you rename them to an empty value. Run it without any arguments to get a quick help.

### vs-replace-text

Replaces text in multiple files. I wrote this to rename entire namespace and object-class prefixes. Combined with `vs-rename` I managed to do over 5000 renames of classes and namespaces (PHP) as well as configuration files. After the renaming (which took less than a couple of minutes) all the unit tests passed.

### vs-sync

A handy too to synchronize the same folder between two machines. It uses `rsync` underneath but removes the hassle of chosing the remote folder to rsync to. For example, if you have a machine at work and want to synchronize your project's folder to your laptop simply open a terminal on your project's root folder and type

	vs-sync to username@your.laptop

and it will synchronize everything. Note that it does not delete missing files/folders on the destination and only updates newer files by default. But run `vs-sync --help` for more help on how to change this behavior or any other options.
