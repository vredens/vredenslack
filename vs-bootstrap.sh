#!/bin/bash

set -e

root_folder=${HOME}/.vredenslack

if [ "$(id -u)" != "0" ]; then
	root_folder=/usr/local/libexec/vredenslack
fi

if [ ! -d ${root_folder} ]; then
	echo "Creating folder: ${root_folder}"
	mkdir ${root_folder}
fi

pushd ${root_folder}

if [ -d vscripts ]; then
	read -p "path ${root_folder}/vscripts already exists. type 'yes' to remove first? " __answer
	if [ "$__answer" == "yes" ]; then
		rm -rf ${root_folder}/vscripts
		echo "Cloning vredenslack repository to ${HOME}/.vredenslack/vslack"
		git clone https://gitlab.com/vredens/vredenslack.git vscripts
	fi
	pushd vscripts
	git pull
else
	echo "Cloning vredenslack repository to ${HOME}/.vredenslack/vslack"
	git clone https://gitlab.com/vredens/vredenslack.git vscripts
	pushd vscripts
fi

echo "Running [${root_folder}/vscripts/bin/vs-self install vscripts] which will install the tool scripts"
./bin/vs-self install vscripts $*
