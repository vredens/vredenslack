#!/bin/bash

case "$1" in
	'uninstall')
		case "$2" in
			'vs-pm')
				#[ -d /etc/vredenslack ] && rm -r /etc/vredenslack
				#[ -d /var/lib/vredenslack ] && rm -r /var/lib/vredenslack
				[ -d /usr/local/libexec/vslack/vs-pm ] && rm -r /usr/local/libexec/vslack/vs-pm
				[ -d /usr/local/libexec/vredenslack/vs-pm ] && rm -r /usr/local/libexec/vredenslack/vs-pm
				cpanm -U Vredenslack::PackageManager
			;;

			'vs-cm')
				[ -d /etc/vredenslack ] && rm -r /etc/vredenslack
				cpanm -U Vredenslack
			;;

			'vscripts')
				rm -r /usr/local/bin/vs-*
				[ -d /usr/local/libexec/vslack ] && rm -r /usr/local/libexec/vslack
				[ -d /usr/local/libexec/vredenslack ] && rm -r /usr/local/libexec/vredenslack
			;;

			*)
				echo "Usage: $0 uninstall vs-pm|vs-cm|vscripts"
			;;
		esac
	;;

	'init')
		vs-pm config init --config-file /etc/vredenslack/pm/config --base-folder /var/lib/vredenslack/pm --slackware-version 14.1
	;;

	'config')
		vs-pm config show --slackware-version 14.1
	;;

	'info')
		vs-pm --slackware-version 14.1 info geany
	;;
esac
