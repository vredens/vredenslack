#!/usr/bin/env perl

use strict;
use warnings;

use Cwd;
use Data::Dumper;
use File::Spec;
use Getopt::Long qw/:config auto_version auto_help/;
use Pod::Usage;
use Term::ANSIColor ':constants';

our $VERSION = '0.2.0';

my $opts = {
	'update' => 1,
	};
GetOptions($opts,
	'update!',
	'delete',
	'dry-run|n',
	'exclude|e=s@',
	'rsync-file|f=s',
	'remote-root|rr=s',
	# help & stuffs
	'version',
	'help',
	'man',
);

if ($opts->{version}) {
	print '::: Version ', $VERSION, "\n";
	exit 0;
}

pod2usage({-verbose => 1}) if $opts->{'help'};
pod2usage({-verbose => 2}) if $opts->{'man'};
pod2usage(1) if @ARGV < 2;

my $cwd = getcwd;
my $dir = shift;
my ($user,$host) = split /@/, $ARGV[0];

die RED, 'do direction provided', RESET unless defined $dir;
die RED, 'invalid direction provided', RESET unless $dir eq 'from' or $dir eq 'to';
die RED, 'no user provided', RESET unless $user;
die RED, 'no host provided', RESET unless $host;

if ($cwd =~ /$ENV{HOME}/ and ! $opts->{'remote-root'}) {
	$cwd =~ s!$ENV{HOME}/!!;
}

my $cmd = 'rsync -az ';
my @opts = ('-az');

push @opts, '--delete' if $opts->{delete};
push @opts, '--update' if $opts->{update};
push @opts, ('--dry-run', '--verbose') if $opts->{'dry-run'};

if (defined $opts->{'rsync-file'}) {
	die RED, 'your rsync file [', $opts->{'rsync-file'}, '] does not exist' unless -f $opts->{'rsync-file'};
	push @opts, '--include-from='.$opts->{'rsync-file'};
}

if ($opts->{exclude}) {
	for (@{$opts->{exclude}}) {
		#die RED, '[', $_, '] is not a folder nor a file' unless -f $_ or -d $_;
		push @opts, "--exclude=$_";
	}
}

my $lf = getcwd . '/';
my $rc = "$user\@$host";
my $rf;
if ($opts->{'remote-root'}) {
	$rf = File::Spec->catfile($opts->{'remote-root'}, $cwd) . '/';
} else {
	$rf = $cwd . '/';
}

# cleanup

$rf =~ s!//!/!g;

if ($dir eq 'to') {
	push @opts, $lf;
	push @opts, $rc . ':"' . $rf . '"';
} else {
	push @opts, $rc . ':"' . $rf . '"';
	push @opts, $lf;
}

my $ev;
print 'running: ', GREEN, 'ssh ', $rc, ' [ -d ', $rf, RESET, " ]\n";
$ev = system('ssh', $rc, '[', '-d', $rf, ']');
$ev >>= 8;
if ($ev != 0) {
	if ($ev == 1) { # no such folder
		if ($dir eq 'to') {
			print 'running: ', GREEN, 'ssh ', $rc, ' mkdir -p ', $rf, RESET, "\n";
			$ev = system('ssh', $rc, 'mkdir', '-p', $rf);
			$ev >>= 8;
			if ($ev != 0) {
				if ($ev < 255) {
					die $ev;
				}
				die $ev;
			}
		} else {
			die 'remote folder does not exist. cannot synchronize from it';
		}
	} elsif ($ev < 255) {
		die 'failed to check if remote folder exists'
	} else {
		die $ev;
	}
}
print 'running: ', GREEN, join(' ', @opts), RESET, "\n";
$ev = system('rsync', @opts);
$ev >>= 8;
if ($ev != 0) {
	if ($ev < 255) {
		die 'remote error';
	} else {
		die $ev;
	}
}

__END__

=head1 NAME

vs-sync - a folder clone between machines

=head1 SYNOPSIS

vs-sync synchronizes a folder from one machine to another using the same folder structure. It uses the I<rsync> command.

  vs-sync [OPTIONS] from|to username@host

This synchronizes the current folder with the remote machine using the same absolute path. If your folder is the home folder then the part of the path which matches your current folder will be striped (also removing the trailing space). This will cause the remote folder to be relative to the remote user. This is useful to sync folders to other users, be them on the same machine or not.

The B<from> and B<to> indicate if files in the remote machine are to be copied to the local machine or vice-versa.

See I<OPTIONS> for a description of the available options which change the default behavior of vs-sync.

=head1 OPTIONS

=head2 -e OR --exclude <file or folder>

Specify a list of directories or files to be ignored. You can provide any number of I<-e> options.

=head2 --rsync-file or -f <file>

Use an rsync include file.

=head2 --delete

You can also force delete of files which are no longer present in the source folder but are present in the destination folder. The default behavior is to NOT delete any files.

=head2 --no-update

By default, files are only synch'ed if they do not exist on the remote site or are newer than the remote files. If you want to copy all local files to the remote location add this option.

=head2 --dry-run or -n

This allows you to do a dry-run. Note that when doing a dry run this means the '--verbose' parameters is automatically passed to rsync.

=head2 -rr OR --remote-root <folder>

Change root remote folder. This will use the remote folder as the root of your remote machine.

=head1 EXAMPLES

=head2 Backup your current folder to a backup folder on a remote host

Note that running as root helps keep file permissions and ownership. It is the recommended when doing backups.

	vs-sync to root@remote.host --delete -rr /backups

This will synchronize the CURRENT FOLDER to the remote host's /backups/<full path to current folder in your machine>/. The full remote path will be created if it does not exist.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in L<http://dev.perl.org/licenses/>.

=cut
